import { Outlet } from "react-router-dom"
import { Fragment } from "react"
import { useSelector, useDispatch } from "react-redux"

import CartIcon from "../../components/cart-icon/cart-icon.component"
import CartDropdown from "../../components/cart-dropdown/cart-dropdown.component"

import { selectOpenCartStatus } from "../../store/cart/cart.selector"
import { selectCurrentUser } from "../../store/user/user.selector"
import { signOutStart } from "../../store/user/user.action"

import { ReactComponent as CrwnLogo } from "../../assets/crown.svg"

import { NavigationContainer, 
        NavLink, 
        NavLinksContainer, 
        LogoContainer } 
        from "./navigation.styles"


const Navigation = () => {
    const currentUser = useSelector(selectCurrentUser)
    const dispatch = useDispatch() 
    const cartOpenClosed = useSelector(selectOpenCartStatus)
    
    const signUserOutHandler = () => {
        dispatch(signOutStart())
    }
   
    return (
        <Fragment>
            <NavigationContainer>
                <LogoContainer to="/">
                    <CrwnLogo />
                </LogoContainer>
                <NavLinksContainer>
                    <NavLink to="/shop">
                        Shop
                    </NavLink>
                    {currentUser ? (
                        <NavLink as="span" onClick={signUserOutHandler}>Sign Out</NavLink>
                         ) : (
                        <NavLink to="/auth">
                            Sign-In
                        </NavLink>
                    )}
                    <CartIcon />
                </NavLinksContainer>
                {cartOpenClosed && <CartDropdown />}
            </NavigationContainer>
            <Outlet />
        </Fragment>
    )
}

export default Navigation