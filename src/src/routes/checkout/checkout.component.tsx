import { CheckoutContainer, CheckoutHeader, HeaderBlock, Total } from "./checkout.styles"
import { useSelector } from "react-redux"

import CheckoutItem from "../../components/checkout-item/checkout-item.component"
import PaymentForm from "../../components/payment-form/payment-form.component"

import { selectCartItems, selectCartTotal } from "../../store/cart/cart.selector"


const Checkout = () => {
    const cartItems = useSelector(selectCartItems)
    const totalCost = useSelector(selectCartTotal)
    
    return(
        <CheckoutContainer>
            <CheckoutHeader>
                <HeaderBlock>
                    <span>Product</span>
                </HeaderBlock>
                <HeaderBlock>
                    <span>Description</span>
                </HeaderBlock>
                <HeaderBlock>
                    <span>Quantity</span>
                </HeaderBlock>
                <HeaderBlock>
                    <span>Price</span>
                </HeaderBlock>
                <HeaderBlock>
                    Remove
                </HeaderBlock>
            </CheckoutHeader>
            {cartItems.map((currentItem) => (
                <CheckoutItem key={currentItem.id} cartItem={currentItem} />
            ))}
            <Total>Total: ${totalCost}</Total>
            <PaymentForm />
        </CheckoutContainer>
    )
}

export default Checkout