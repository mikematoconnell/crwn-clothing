import { CategoryPreviewContainer, Title, Preview} from "./category-preview.styles"

import { FC } from "react"

import ProductCard from "../product-card/product-card.component"

import { useNavigate } from "react-router-dom"

import { CategoryItem } from "../../store/categories/categories.types"

export type CategoryPreviewProps = {
    title: string
    products: CategoryItem[]
}

const CategoryPreview: FC<CategoryPreviewProps> = ({ title, products}) => {
    const navigate = useNavigate()
    const handleNavigate = () => {
        navigate(`${title}`)
    }

    return(
        <CategoryPreviewContainer>
            <h2>
                <Title onClick={handleNavigate}>{title.toUpperCase()}</Title>
            </h2>
            <Preview>
                {
                    products.filter((_, idx) => idx < 4).map((product) => 
                        <ProductCard key={product.id}  product={product}/>
                    )
                }
            </Preview>
        </CategoryPreviewContainer>
    )
}

export default CategoryPreview