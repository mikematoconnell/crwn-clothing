import { useNavigate } from "react-router-dom"
import { CartDropdownContainer, CartItemsStyle, EmptyMessage } from "./cart-dropdown.styles"
import Button from "../button/button.component"
import CartItem from "../cart-item/cart-item.component"

import { useDispatch, useSelector } from "react-redux"
import { setCartOpenClosed } from "../../store/cart/cart.action"
import { selectCartItems, selectOpenCartStatus } from "../../store/cart/cart.selector"


const CartDropdown = () => {
    const cartItems = useSelector(selectCartItems)
    const cartOpenClosed = useSelector(selectOpenCartStatus)
    
    const navigate = useNavigate()
    const dispatch = useDispatch()

    const goToCheckoutHandler = () => {
        dispatch(setCartOpenClosed(cartOpenClosed))
        navigate("/checkout")
    }

    return (
        <CartDropdownContainer>
            <CartItemsStyle>
            {
                cartItems.length ? (cartItems.map(item => 
                    <CartItem key={item.id} cartItem={item} />)
                ) : (
                    <EmptyMessage>Your cart is empty</EmptyMessage>
                )
            }
            </CartItemsStyle>
            <Button onClick={goToCheckoutHandler}>Go To Checkout</Button>
        </CartDropdownContainer>
    )
}

export default CartDropdown