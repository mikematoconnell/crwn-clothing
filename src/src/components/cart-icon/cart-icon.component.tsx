
import { CartIconContainer,  ItemCount, ShoppingIcon} from "./cart-icon.styles"

import { useSelector, useDispatch } from "react-redux"
import { setCartOpenClosed } from "../../store/cart/cart.action"
import { selectNumberOfItems, selectOpenCartStatus } from "../../store/cart/cart.selector"

const CartIcon = () => {
    const numberOfCartItems = useSelector(selectNumberOfItems)
    const dispatch = useDispatch()
    const cartOpenClosed = useSelector(selectOpenCartStatus)


    function handleOpenClosedCart(){
        dispatch(setCartOpenClosed(cartOpenClosed))
    }
    return (
        <CartIconContainer onClick={handleOpenClosedCart}>
            <ShoppingIcon  />
            <ItemCount data-testid="numberOfCartItems">{numberOfCartItems}</ItemCount>
        </CartIconContainer>
    )
}

export default CartIcon