import { CartItemContainer, ItemDetails } from "./cart-item.styles"

import { FC } from "react"

import { CartItem } from "../../store/cart/cart.types"

export type CartItemProps = {
    cartItem: CartItem
}

const CartItemComponent: FC<CartItemProps> = ({cartItem}) => {
    const { name, imageUrl, quantity, price } = cartItem
    return (
        <CartItemContainer>
            <img src={imageUrl} alt={`${name}`} />
            <ItemDetails>
                <span>{name}</span>
                <span>{quantity} x ${price} </span>
            </ItemDetails>
        </CartItemContainer>
    )
}

export default CartItemComponent