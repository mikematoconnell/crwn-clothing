import { CheckoutItemContainer, 
    ImageContainer,
    BaseSpan,
    Quantity,
    Arrow,
    Value,
    RemoveButton } 
    from "./checkout-item.styles"

import { FC } from "react"

import { CartItem } from "../../store/cart/cart.types"

import { useDispatch, useSelector } from "react-redux"
import { removeItem, addItemToCart, decreaseItem } from "../../store/cart/cart.action"
import { selectCartItems } from "../../store/cart/cart.selector"

export type CheckoutItemProps = {
    cartItem: CartItem
}

const CheckoutItem: FC<CheckoutItemProps> = ({cartItem}) => {
    const { name, imageUrl, price, quantity } = cartItem
    const dispatch = useDispatch()
    const cartItems = useSelector(selectCartItems)

    const deleteItemHandler = () => dispatch(removeItem(cartItems, cartItem))
    const addItemHandler = () => dispatch(addItemToCart(cartItems, cartItem))
    const decreaseItemHandler = () => dispatch(decreaseItem(cartItems, cartItem))

    return (
        <CheckoutItemContainer>
            <ImageContainer>
                <img src={imageUrl} alt={`${name}`} />
            </ImageContainer>
            <BaseSpan>{name}</BaseSpan>
            <Quantity>
                <Arrow onClick={ (decreaseItemHandler) }>
                    &#10094;
                </Arrow>
                    <Value>{quantity}</Value>
                <Arrow onClick={ (addItemHandler) }>
                    &#10095;
                </Arrow>
            </Quantity>
            <BaseSpan>{price}</BaseSpan>
            <RemoveButton onClick={ (deleteItemHandler) }>&#10005;</RemoveButton>
        </CheckoutItemContainer>
    )
}

export default CheckoutItem