import { CART_ACTION_TYPES } from "./cart.types";
import { createAction, ActionWithPayload, withMatcher } from "../../utils/reducer/reducer.utils";

import { CartItem } from "./cart.types";
import { CategoryItem } from "../categories/categories.types";


const addToCart = (cartItems: CartItem[], itemToAdd: CategoryItem): CartItem[] => {
    const existingCartItem = cartItems.find(
        (cartItem) => cartItem.id === itemToAdd.id
    )
    
    if(existingCartItem) {
        return cartItems.map((cartItem) => 
        cartItem.id === itemToAdd.id
        ? { ...cartItem, quantity: cartItem.quantity + 1}
        : cartItem)
    }

    return [ ...cartItems, { ...itemToAdd, quantity: 1}]
}

const removeCartItem = (cartItems: CartItem[], itemToRemove: CategoryItem) => {
    return cartItems.filter((cartItem) => (
        cartItem.id !== itemToRemove.id
    ))
} 

const decreaseCartItem = (cartItems: CartItem[], itemToDecrease: CategoryItem) => {
    const existingItem = cartItems.find((cartItem) => (
        cartItem.id === itemToDecrease.id
    ))
    if (existingItem && existingItem.quantity === 1) {
        return removeCartItem(cartItems, itemToDecrease)
    }
    
    return cartItems.map((cartItem) => (
        cartItem.id === itemToDecrease.id ? {...cartItem, quantity: cartItem.quantity - 1}
        : cartItem
    ))
}

export type SetIsCartOpen = ActionWithPayload<CART_ACTION_TYPES.SET_OPEN_STATUS, boolean>

export type SetCartItems = ActionWithPayload<CART_ACTION_TYPES.SET_CART_ITEMS, CartItem[]>

export const setCartItems = withMatcher((cartItems: CartItem[]): SetCartItems => createAction(CART_ACTION_TYPES.SET_CART_ITEMS, cartItems))

export const addItemToCart = (cartItems: CartItem[], productToAdd: CategoryItem): SetCartItems => {
    const newCartItems = addToCart(cartItems, productToAdd)
    return setCartItems(newCartItems)
}

export const decreaseItem = (cartItems: CartItem[], productToDecrease: CategoryItem) => {
    const newCartItems = decreaseCartItem(cartItems, productToDecrease)
    return setCartItems(newCartItems)
}

export const removeItem = (cartItems: CartItem[], productToRemove: CategoryItem) => {
    const newCartItems = removeCartItem(cartItems, productToRemove);
    return setCartItems(newCartItems);
}

export const setCartOpenClosed = withMatcher((currentOpenValue: boolean): SetIsCartOpen => (
    createAction(CART_ACTION_TYPES.SET_OPEN_STATUS, currentOpenValue)
))


