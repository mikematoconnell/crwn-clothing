import { AnyAction } from "redux"
import { CartItem } from "./cart.types"

import { setCartItems, setCartOpenClosed } from "./cart.action"

export type CartState = {
    readonly cartOpenClosed: boolean,
    readonly cartItems: CartItem[],
}

const INITIAL_STATE: CartState = {
    cartOpenClosed: false,
    cartItems: [],

}

export const cartReducer = (state = INITIAL_STATE, action: AnyAction) => {
    if(setCartItems.match(action)){
        return {
            ...state,
            cartItems: action.payload
        }
    }

    if(setCartOpenClosed.match(action)) {
        return {
            ...state,
            cartOpenClosed: !action.payload
        }
    }

    return state
}