import { CategoryItem } from "../categories/categories.types";

export enum CART_ACTION_TYPES {
    SET_OPEN_STATUS = "cart/SET_OPEN_STATUS",
    SET_CART_ITEMS = "cart/SET_CART_ITEMS",
}

export type CartItem = CategoryItem & {
    quantity: number;
}

