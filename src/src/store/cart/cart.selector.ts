import { createSelector } from "reselect"

import { CartState } from "./cart.reducer"

import { RootState } from "../store"

const selectCartReducer = (state: RootState): CartState => state.cart

export const selectOpenCartStatus = createSelector(
    [selectCartReducer],
    (cartSlice) => cartSlice.cartOpenClosed
)

export const selectCartItems = createSelector(
    [selectCartReducer],
    (cartSlice) => cartSlice.cartItems
)

export const selectNumberOfItems = createSelector(
    [selectCartItems],
    (cartItems) => cartItems.reduce((total, cartItem) => total += cartItem.quantity, 0)
)

export const selectCartTotal = createSelector(
    [selectCartItems],
    (cartItems) => cartItems.reduce((totalCost, cartItem) => totalCost += (cartItem.quantity * cartItem.price), 0)
)