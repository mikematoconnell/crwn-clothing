import { CATEGORIES_ACTION_TYPES } from './categories.types';

import { createAction, withMatcher, ActionWithPayload, ActionWithoutPayload } from '../../utils/reducer/reducer.utils';

import { Category } from './categories.types';


export type FETCH_CATEGORIES_START = ActionWithoutPayload<CATEGORIES_ACTION_TYPES.FETCH_CATEGORIES_START>

export type FETCH_CATEGORIES_SUCCESS = ActionWithPayload<CATEGORIES_ACTION_TYPES.FETCH_CATEGORIES_SUCCESS, Category[]>

export type FETCH_CATEGORIES_FAILED = ActionWithPayload<CATEGORIES_ACTION_TYPES.FETCH_CATEGORIES_FAILED, Error>


export const fetchCategoriesStart = withMatcher((): FETCH_CATEGORIES_START =>
  createAction(CATEGORIES_ACTION_TYPES.FETCH_CATEGORIES_START));

export const fetchCategoriesSuccess = withMatcher((categoriesArray: Category[]): FETCH_CATEGORIES_SUCCESS =>
  createAction(
    CATEGORIES_ACTION_TYPES.FETCH_CATEGORIES_SUCCESS,
    categoriesArray
  ));

export const fetchCategoriesFailure = withMatcher((error: Error): FETCH_CATEGORIES_FAILED =>
  createAction(CATEGORIES_ACTION_TYPES.FETCH_CATEGORIES_FAILED, error));

